const amqp = require('amqplib');
const nodemailer = require('nodemailer');
const { google} = require('googleapis');
const config = require("./config");
const OAuth2 = google.auth.OAuth2

const OAuth2_client = new OAuth2(config.clientId, config.clientSecret)
OAuth2_client.setCredentials({ refresh_token : config.refreshToken })

function sendMail(to, subject, text) {
  const accessToken = OAuth2_client.getAccessToken()
  
    // Configuration du service d'email
  const transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
      type: 'OAuth2',
      user: config.user,
      clientId: config.clientId,
      clientSecret: config.clientSecret,
      refreshToken: config.refreshToken,
      accessToken: accessToken
    }
  });

  const mailOptions = {
    from: `me <${config.user}>`,
    to: to,
    subject: subject,
    text: text
  };

  transporter.sendMail(mailOptions, function(error, result) {
    if(error){
      console.log(`Error :`,error)
    }else{
      console.log(`Success :`,result)

    }
    transporter.close()
  });

}

// Connexion au message broker RabbitMQ
amqp.connect('amqp://user:password@rabbitmqhost:5672')
  .then(conn => {
    return conn.createChannel();
  })
  .then(ch => {
    const queue = 'payment_confirmation';
    ch.assertQueue(queue, { durable: true });
    console.log(`En attente de messages dans la file ${queue}.`);
    ch.consume(queue, async (msg) => {
      const message = JSON.parse(msg.content.toString());
      const to = message.data.to;
      const subject = message.data.subject;
      const text = message.data.text;
      await sendMail(to, subject, text);
      console.log(`Email envoyé à ${to}`);
      ch.ack(msg);
    });
  })
  .catch(err => {
    console.log(`Erreur: ${err}`);
  });
